#!/bin/sh
# This script was generated using Makeself 2.3.0

ORIG_UMASK=`umask`
if test "n" = n; then
    umask 077
fi

CRCsum="1360513082"
MD5="fd66079de08609d061a3becc804e3549"
TMPROOT=${TMPDIR:=/tmp}
USER_PWD="$PWD"; export USER_PWD

label="Veeam PN installer"
script="./startup.sh"
scriptargs=""
licensetxt=""
helpheader=''
targetdir="makeself-1-20191122113334"
filesizes="55335"
keep="n"
nooverwrite="n"
quiet="n"

print_cmd_arg=""
if type printf > /dev/null; then
    print_cmd="printf"
elif test -x /usr/ucb/echo; then
    print_cmd="/usr/ucb/echo"
else
    print_cmd="echo"
fi

unset CDPATH

MS_Printf()
{
    $print_cmd $print_cmd_arg "$1"
}

MS_PrintLicense()
{
  if test x"$licensetxt" != x; then
    echo "$licensetxt"
    while true
    do
      MS_Printf "Please type y to accept, n otherwise: "
      read yn
      if test x"$yn" = xn; then
        keep=n
	eval $finish; exit 1
        break;
      elif test x"$yn" = xy; then
        break;
      fi
    done
  fi
}

MS_diskspace()
{
	(
	if test -d /usr/xpg4/bin; then
		PATH=/usr/xpg4/bin:$PATH
	fi
	df -kP "$1" | tail -1 | awk '{ if ($4 ~ /%/) {print $3} else {print $4} }'
	)
}

MS_dd()
{
    blocks=`expr $3 / 1024`
    bytes=`expr $3 % 1024`
    dd if="$1" ibs=$2 skip=1 obs=1024 conv=sync 2> /dev/null | \
    { test $blocks -gt 0 && dd ibs=1024 obs=1024 count=$blocks ; \
      test $bytes  -gt 0 && dd ibs=1 obs=1024 count=$bytes ; } 2> /dev/null
}

MS_dd_Progress()
{
    if test x"$noprogress" = xy; then
        MS_dd $@
        return $?
    fi
    file="$1"
    offset=$2
    length=$3
    pos=0
    bsize=4194304
    while test $bsize -gt $length; do
        bsize=`expr $bsize / 4`
    done
    blocks=`expr $length / $bsize`
    bytes=`expr $length % $bsize`
    (
        dd ibs=$offset skip=1 2>/dev/null
        pos=`expr $pos \+ $bsize`
        MS_Printf "     0%% " 1>&2
        if test $blocks -gt 0; then
            while test $pos -le $length; do
                dd bs=$bsize count=1 2>/dev/null
                pcent=`expr $length / 100`
                pcent=`expr $pos / $pcent`
                if test $pcent -lt 100; then
                    MS_Printf "\b\b\b\b\b\b\b" 1>&2
                    if test $pcent -lt 10; then
                        MS_Printf "    $pcent%% " 1>&2
                    else
                        MS_Printf "   $pcent%% " 1>&2
                    fi
                fi
                pos=`expr $pos \+ $bsize`
            done
        fi
        if test $bytes -gt 0; then
            dd bs=$bytes count=1 2>/dev/null
        fi
        MS_Printf "\b\b\b\b\b\b\b" 1>&2
        MS_Printf " 100%%  " 1>&2
    ) < "$file"
}

MS_Help()
{
    cat << EOH >&2
${helpheader}Makeself version 2.3.0
 1) Getting help or info about $0 :
  $0 --help   Print this message
  $0 --info   Print embedded info : title, default target directory, embedded script ...
  $0 --lsm    Print embedded lsm entry (or no LSM)
  $0 --list   Print the list of files in the archive
  $0 --check  Checks integrity of the archive

 2) Running $0 :
  $0 [options] [--] [additional arguments to embedded script]
  with following options (in that order)
  --confirm             Ask before running embedded script
  --quiet		Do not print anything except error messages
  --noexec              Do not run embedded script
  --keep                Do not erase target directory after running
			the embedded script
  --noprogress          Do not show the progress during the decompression
  --nox11               Do not spawn an xterm
  --nochown             Do not give the extracted files to the current user
  --target dir          Extract directly to a target directory
                        directory path can be either absolute or relative
  --tar arg1 [arg2 ...] Access the contents of the archive through the tar command
  --                    Following arguments will be passed to the embedded script
EOH
}

MS_Check()
{
    OLD_PATH="$PATH"
    PATH=${GUESS_MD5_PATH:-"$OLD_PATH:/bin:/usr/bin:/sbin:/usr/local/ssl/bin:/usr/local/bin:/opt/openssl/bin"}
	MD5_ARG=""
    MD5_PATH=`exec <&- 2>&-; which md5sum || command -v md5sum || type md5sum`
    test -x "$MD5_PATH" || MD5_PATH=`exec <&- 2>&-; which md5 || command -v md5 || type md5`
	test -x "$MD5_PATH" || MD5_PATH=`exec <&- 2>&-; which digest || command -v digest || type digest`
    PATH="$OLD_PATH"

    if test x"$quiet" = xn; then
		MS_Printf "Verifying archive integrity..."
    fi
    offset=`head -n 522 "$1" | wc -c | tr -d " "`
    verb=$2
    i=1
    for s in $filesizes
    do
		crc=`echo $CRCsum | cut -d" " -f$i`
		if test -x "$MD5_PATH"; then
			if test x"`basename $MD5_PATH`" = xdigest; then
				MD5_ARG="-a md5"
			fi
			md5=`echo $MD5 | cut -d" " -f$i`
			if test x"$md5" = x00000000000000000000000000000000; then
				test x"$verb" = xy && echo " $1 does not contain an embedded MD5 checksum." >&2
			else
				md5sum=`MS_dd_Progress "$1" $offset $s | eval "$MD5_PATH $MD5_ARG" | cut -b-32`;
				if test x"$md5sum" != x"$md5"; then
					echo "Error in MD5 checksums: $md5sum is different from $md5" >&2
					exit 2
				else
					test x"$verb" = xy && MS_Printf " MD5 checksums are OK." >&2
				fi
				crc="0000000000"; verb=n
			fi
		fi
		if test x"$crc" = x0000000000; then
			test x"$verb" = xy && echo " $1 does not contain a CRC checksum." >&2
		else
			sum1=`MS_dd_Progress "$1" $offset $s | CMD_ENV=xpg4 cksum | awk '{print $1}'`
			if test x"$sum1" = x"$crc"; then
				test x"$verb" = xy && MS_Printf " CRC checksums are OK." >&2
			else
				echo "Error in checksums: $sum1 is different from $crc" >&2
				exit 2;
			fi
		fi
		i=`expr $i + 1`
		offset=`expr $offset + $s`
    done
    if test x"$quiet" = xn; then
		echo " All good."
    fi
}

UnTAR()
{
    if test x"$quiet" = xn; then
		tar $1vf - 2>&1 || { echo Extraction failed. > /dev/tty; kill -15 $$; }
    else

		tar $1f - 2>&1 || { echo Extraction failed. > /dev/tty; kill -15 $$; }
    fi
}

finish=true
xterm_loop=
noprogress=n
nox11=n
copy=none
ownership=y
verbose=n

initargs="$@"

while true
do
    case "$1" in
    -h | --help)
	MS_Help
	exit 0
	;;
    -q | --quiet)
	quiet=y
	noprogress=y
	shift
	;;
    --info)
	echo Identification: "$label"
	echo Target directory: "$targetdir"
	echo Uncompressed size: 48 KB
	echo Compression: base64
	echo Date of packaging: Fri Nov 22 11:33:34 UTC 2019
	echo Built with Makeself version 2.3.0 on 
	echo Build command was: "/usr/bin/makeself \\
    \"--base64\" \\
    \".\" \\
    \"VeeamPN-installer.run.sh\" \\
    \"Veeam PN installer\" \\
    \"./startup.sh\""
	if test x"$script" != x; then
	    echo Script run after extraction:
	    echo "    " $script $scriptargs
	fi
	if test x"" = xcopy; then
		echo "Archive will copy itself to a temporary location"
	fi
	if test x"n" = xy; then
		echo "Root permissions required for extraction"
	fi
	if test x"n" = xy; then
	    echo "directory $targetdir is permanent"
	else
	    echo "$targetdir will be removed after extraction"
	fi
	exit 0
	;;
    --dumpconf)
	echo LABEL=\"$label\"
	echo SCRIPT=\"$script\"
	echo SCRIPTARGS=\"$scriptargs\"
	echo archdirname=\"makeself-1-20191122113334\"
	echo KEEP=n
	echo NOOVERWRITE=n
	echo COMPRESS=base64
	echo filesizes=\"$filesizes\"
	echo CRCsum=\"$CRCsum\"
	echo MD5sum=\"$MD5\"
	echo OLDUSIZE=48
	echo OLDSKIP=523
	exit 0
	;;
    --lsm)
cat << EOLSM
No LSM.
EOLSM
	exit 0
	;;
    --list)
	echo Target directory: $targetdir
	offset=`head -n 522 "$0" | wc -c | tr -d " "`
	for s in $filesizes
	do
	    MS_dd "$0" $offset $s | eval "base64 -d -i" | UnTAR t
	    offset=`expr $offset + $s`
	done
	exit 0
	;;
	--tar)
	offset=`head -n 522 "$0" | wc -c | tr -d " "`
	arg1="$2"
    if ! shift 2; then MS_Help; exit 1; fi
	for s in $filesizes
	do
	    MS_dd "$0" $offset $s | eval "base64 -d -i" | tar "$arg1" - "$@"
	    offset=`expr $offset + $s`
	done
	exit 0
	;;
    --check)
	MS_Check "$0" y
	exit 0
	;;
    --confirm)
	verbose=y
	shift
	;;
	--noexec)
	script=""
	shift
	;;
    --keep)
	keep=y
	shift
	;;
    --target)
	keep=y
	targetdir=${2:-.}
    if ! shift 2; then MS_Help; exit 1; fi
	;;
    --noprogress)
	noprogress=y
	shift
	;;
    --nox11)
	nox11=y
	shift
	;;
    --nochown)
	ownership=n
	shift
	;;
    --xwin)
	if test "n" = n; then
		finish="echo Press Return to close this window...; read junk"
	fi
	xterm_loop=1
	shift
	;;
    --phase2)
	copy=phase2
	shift
	;;
    --)
	shift
	break ;;
    -*)
	echo Unrecognized flag : "$1" >&2
	MS_Help
	exit 1
	;;
    *)
	break ;;
    esac
done

if test x"$quiet" = xy -a x"$verbose" = xy; then
	echo Cannot be verbose and quiet at the same time. >&2
	exit 1
fi

if test x"n" = xy -a `id -u` -ne 0; then
	echo "Administrative privileges required for this archive (use su or sudo)" >&2
	exit 1	
fi

if test x"$copy" \!= xphase2; then
    MS_PrintLicense
fi

case "$copy" in
copy)
    tmpdir=$TMPROOT/makeself.$RANDOM.`date +"%y%m%d%H%M%S"`.$$
    mkdir "$tmpdir" || {
	echo "Could not create temporary directory $tmpdir" >&2
	exit 1
    }
    SCRIPT_COPY="$tmpdir/makeself"
    echo "Copying to a temporary location..." >&2
    cp "$0" "$SCRIPT_COPY"
    chmod +x "$SCRIPT_COPY"
    cd "$TMPROOT"
    exec "$SCRIPT_COPY" --phase2 -- $initargs
    ;;
phase2)
    finish="$finish ; rm -rf `dirname $0`"
    ;;
esac

if test x"$nox11" = xn; then
    if tty -s; then                 # Do we have a terminal?
	:
    else
        if test x"$DISPLAY" != x -a x"$xterm_loop" = x; then  # No, but do we have X?
            if xset q > /dev/null 2>&1; then # Check for valid DISPLAY variable
                GUESS_XTERMS="xterm gnome-terminal rxvt dtterm eterm Eterm xfce4-terminal lxterminal kvt konsole aterm terminology"
                for a in $GUESS_XTERMS; do
                    if type $a >/dev/null 2>&1; then
                        XTERM=$a
                        break
                    fi
                done
                chmod a+x $0 || echo Please add execution rights on $0
                if test `echo "$0" | cut -c1` = "/"; then # Spawn a terminal!
                    exec $XTERM -title "$label" -e "$0" --xwin "$initargs"
                else
                    exec $XTERM -title "$label" -e "./$0" --xwin "$initargs"
                fi
            fi
        fi
    fi
fi

if test x"$targetdir" = x.; then
    tmpdir="."
else
    if test x"$keep" = xy; then
	if test x"$nooverwrite" = xy && test -d "$targetdir"; then
            echo "Target directory $targetdir already exists, aborting." >&2
            exit 1
	fi
	if test x"$quiet" = xn; then
	    echo "Creating directory $targetdir" >&2
	fi
	tmpdir="$targetdir"
	dashp="-p"
    else
	tmpdir="$TMPROOT/selfgz$$$RANDOM"
	dashp=""
    fi
    mkdir $dashp $tmpdir || {
	echo 'Cannot create target directory' $tmpdir >&2
	echo 'You should try option --target dir' >&2
	eval $finish
	exit 1
    }
fi

location="`pwd`"
if test x"$SETUP_NOCHECK" != x1; then
    MS_Check "$0"
fi
offset=`head -n 522 "$0" | wc -c | tr -d " "`

if test x"$verbose" = xy; then
	MS_Printf "About to extract 48 KB in $tmpdir ... Proceed ? [Y/n] "
	read yn
	if test x"$yn" = xn; then
		eval $finish; exit 1
	fi
fi

if test x"$quiet" = xn; then
	MS_Printf "Uncompressing $label"
fi
res=3
if test x"$keep" = xn; then
    trap 'echo Signal caught, cleaning up >&2; cd $TMPROOT; /bin/rm -rf $tmpdir; eval $finish; exit 15' 1 2 3 15
fi

leftspace=`MS_diskspace $tmpdir`
if test -n "$leftspace"; then
    if test "$leftspace" -lt 48; then
        echo
        echo "Not enough space left in "`dirname $tmpdir`" ($leftspace KB) to decompress $0 (48 KB)" >&2
        if test x"$keep" = xn; then
            echo "Consider setting TMPDIR to a directory with more free space."
        fi
        eval $finish; exit 1
    fi
fi

for s in $filesizes
do
    if MS_dd_Progress "$0" $offset $s | eval "base64 -d -i" | ( cd "$tmpdir"; umask $ORIG_UMASK ; UnTAR xp ) 1>/dev/null; then
		if test x"$ownership" = xy; then
			(PATH=/usr/xpg4/bin:$PATH; cd "$tmpdir"; chown -R `id -u` .;  chgrp -R `id -g` .)
		fi
    else
		echo >&2
		echo "Unable to decompress $0" >&2
		eval $finish; exit 1
    fi
    offset=`expr $offset + $s`
done
if test x"$quiet" = xn; then
	echo
fi

cd "$tmpdir"
res=0
if test x"$script" != x; then
    if test x"$verbose" = x"y"; then
		MS_Printf "OK to execute: $script $scriptargs $* ? [Y/n] "
		read yn
		if test x"$yn" = x -o x"$yn" = xy -o x"$yn" = xY; then
			eval "\"$script\" $scriptargs \"\$@\""; res=$?;
		fi
    else
		eval "\"$script\" $scriptargs \"\$@\""; res=$?
    fi
    if test "$res" -ne 0; then
		test x"$verbose" = xy && echo "The program '$script' returned an error code ($res)" >&2
    fi
fi
if test x"$keep" = xn; then
    cd $TMPROOT
    /bin/rm -rf $tmpdir
fi
eval $finish; exit $res
Li8AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAADAwMDA3NTUAMDAwMDAw
MAAwMDAwMDAwADAwMDAwMDAwMDAwADEzNTY1NzQzNjAxADAwNzcyMwAgNQAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAB1c3RhciAgAHJvb3QAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAcm9vdAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAu
L3dlYl9sb2cuc2gAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAMDAwMDc1NQAwMDAwMDAw
ADAwMDAwMDAAMDAwMDAwMDI2NzYAMTM1NjU3NDM2MDEAMDExNzEzACAwAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAHVzdGFyICAAcm9vdAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAByb290AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACMh
L3Vzci9iaW4vZW52IGJhc2gKCiNzaGVsbGNoZWNrIGRpc2FibGU9U0MyMDE1CgpbWyAtbiAkSEFW
RV9XRUJfTE9HIF1dICYmIHJldHVybiB8fCByZWFkb25seSBIQVZFX1dFQl9MT0c9MQoKcmVhZG9u
bHkgVkVFQU1QTl9XRUJfTE9HX0ZJTEU9L3Zhci9sb2cvVmVlYW0vdmVlYW1wbi93ZWIubG9nLnR4
dAoKZnVuY3Rpb24gd2ViX2xvZ19pbml0aWFsaXplCnsKICAgIGlmIFtbICEgLWYgJFZFRUFNUE5f
V0VCX0xPR19GSUxFIF1dOyB0aGVuCiAgICAgICAgaW5zdGFsbCAtbSAwNzU1IC1vIHJvb3QgLWcg
cm9vdCAtZCAiJChkaXJuYW1lICIkVkVFQU1QTl9XRUJfTE9HX0ZJTEUiKSIKICAgICAgICBpbnN0
YWxsIC1tIDA2NDQgLW8gcm9vdCAtZyByb290IC9kZXYvbnVsbCAiJFZFRUFNUE5fV0VCX0xPR19G
SUxFIgogICAgZmkKfQoKZnVuY3Rpb24gd2ViX2xvZ19maW5hbGl6ZQp7CiAgICBpZiBbWyAtZiAk
VkVFQU1QTl9XRUJfTE9HX0ZJTEUgXV07IHRoZW4KICAgICAgICBzbGVlcCA2ICMgc2xlZXAgZm9y
IGEgd2hpbGUgdG8gZ2l2ZSB2ZWVhbS1pbml0IHNpdGUgYSBjaGFuY2UgZm9yIHVwZGF0ZQogICAg
ICAgIG12IC1mICIkVkVFQU1QTl9XRUJfTE9HX0ZJTEUiICIke1ZFRUFNUE5fV0VCX0xPR19GSUxF
Ly50eHQvLmZpbmlzaGVkfSIKICAgIGZpCn0KCmZ1bmN0aW9uIHdlYl9sb2dfZXNjYXBlCnsKICAg
IGVjaG8gLW4gIiQqIiB8IHNlZCAncy8iL1xcIi9nJwp9CgpmdW5jdGlvbiB3ZWJfbG9nCnsKICAg
IFtbIC1mICRWRUVBTVBOX1dFQl9MT0dfRklMRSBdXSAmJiBjYXQgPj4gIiRWRUVBTVBOX1dFQl9M
T0dfRklMRSIgPDxFT0YKeyAidGltZSI6ICQoZGF0ZSAtdSArJyVzJTNOJyksICJ0eXBlIjogImlu
Zm8iLCAidGV4dCI6ICIkKHdlYl9sb2dfZXNjYXBlICIkKiIpIiB9CkVPRgp9CgpmdW5jdGlvbiB3
ZWJfbG9nX2xhc3QKewogICAgW1sgLWYgJFZFRUFNUE5fV0VCX0xPR19GSUxFIF1dICYmIGNhdCA+
PiAiJFZFRUFNUE5fV0VCX0xPR19GSUxFIiA8PEVPRgp7ICJ0aW1lIjogJChkYXRlIC11ICsnJXMl
M04nKSwgInR5cGUiOiAibGFzdCIsICJ0ZXh0IjogIiQod2ViX2xvZ19lc2NhcGUgIiQqIikiIH0K
RU9GCn0KCmZ1bmN0aW9uIHdlYl9sb2dfZmFpbAp7CiAgICBbWyAtZiAkVkVFQU1QTl9XRUJfTE9H
X0ZJTEUgXV0gJiYgY2F0ID4+ICIkVkVFQU1QTl9XRUJfTE9HX0ZJTEUiIDw8RU9GCnsgInRpbWUi
OiAkKGRhdGUgLXUgKyclcyUzTicpLCAidHlwZSI6ICJmYWlsIiwgInRleHQiOiAiJCh3ZWJfbG9n
X2VzY2FwZSAiJCoiKSIgfQpFT0YKfQoKZnVuY3Rpb24gd2ViX2xvZ193YXJuaW5nCnsKICAgIFtb
IC1mICRWRUVBTVBOX1dFQl9MT0dfRklMRSBdXSAmJiBjYXQgPj4gIiRWRUVBTVBOX1dFQl9MT0df
RklMRSIgPDxFT0YKeyAidGltZSI6ICQoZGF0ZSAtdSArJyVzJTNOJyksICJ0eXBlIjogIndhcm4i
LCAidGV4dCI6ICIkKHdlYl9sb2dfZXNjYXBlICIkKiIpIiB9CkVPRgp9CgAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAC4vZmF2
aWNvbi5pY28AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAwMDAwNjQ0ADAwMDAwMDAAMDAw
MDAwMAAwMDAwMDAwMjE3NgAxMzU2NTc0MzYwMQAwMTIwNTIAIDAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAdXN0YXIgIAByb290AAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAHJvb3QAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABAAEA
EBAAAAEAIABoBAAAFgAAACgAAAAQAAAAIAAAAAEAIAAAAAAAQAQAAAAAAAAAAAAAAAAAAAAAAABa
UT3/WlE9/1pRPf9aUT3/WlE9/1pRPf9aUT3/WlE9/1pRPf9aUT3/WlE9/1pRPf9aUT3/WlE9/1pR
Pf9aUT3/WlE9/1pRPf9aUT3/WlE9/1pRPf9aUT3/YllG/11UQP9dVEH/YllG/1pRPf9aUT3/WlE9
/1pRPf9aUT3/WlE9/1pRPf9aUT3/WlE9/1pRPf9aUT3/YllG/5GMfv/5+fj/+vr5/5GMfv9iWUb/
WlE9/1pRPf9aUT3/WlE9/1pRPf9aUT3/WlE9/1pRPf9aUT3/WlE9/1tSPv/s6+j////////////s
6+j/W1I+/1pRPf9aUT3/WlE9/1pRPf9aUT3/WlE9/1pRPf9aUT3/WlE9/1pRPf9eVUL/3dvX////
////////3dvX/15VQv9aUT3/WlE9/1pRPf9aUT3/WlE9/1pRPf9aUT3/WlE9/1pRPf9aUT3/X1ZD
/3NrWv/Oy8X/zcrE/3JrWv9fVkP/WlE9/1pRPf9aUT3/WlE9/1pRPf9aUT3/WlE9/1pRPf9aUT3/
WlE9/5aQhP+zrqX/bGRS/2xkUv+zrqX/lpCE/1pRPf9aUT3/WlE9/1pRPf9aUT3/WlE9/1pRPf9a
UT3/WlE9/2BXRP/t7Or/6ujm/15VQv9eVUL/6ujm/+3s6v9gV0T/WlE9/1pRPf9aUT3/WlE9/1pR
Pf9aUT3/WlE9/1pRPf+opJn//////5KMfv9aUT3/WlE9/5KMfv//////qKSZ/1pRPf9aUT3/WlE9
/1pRPf9aUT3/WlE9/2JaR/9kW0j/kIl8/8vIwv9bUj7/WlE9/1pRPf9bUj7/zMnD/5CJfP9kW0j/
Y1pH/1pRPf9aUT3/WlE9/15VQf+zrqX/29nU/6GbkP9jW0j/WlE9/1pRPf9aUT3/WlE9/2NaSP+h
m5D/29nV/7GtpP9eVUH/WlE9/1xTQP+XkoX/////////////////fHRk/5yWiv/Cvrf/wr63/5yW
i/98dGT/////////////////l5KF/1xTP/9dVEH/pJ+U/////////////////4uFd/+1san/9fX0
//X19P+1san/jIV3/////////////////6Wglf9eVUL/WlE9/2RcSf/j4d7//////9LQy/9dVUH/
WlE9/1pRPf9aUT3/WlE9/11UQP/U0cz//////+Ph3v9jW0j/WlE9/1pRPf9dVEH/XVVB/2JZRv9f
VkL/W1I+/1pRPf9aUT3/WlE9/1pRPf9bUj7/X1ZC/2JZRv9dVUH/XVRB/1pRPf9aUT3/WlE9/1pR
Pf9aUT3/WlE9/1pRPf9aUT3/WlE9/1pRPf9aUT3/WlE9/1pRPf9aUT3/WlE9/1pRPf9aUT3/AAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAALi9pbmRleC5o
dG1sAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAADAwMDA2NDQAMDAwMDAwMAAwMDAwMDAw
ADAwMDAwMDE2NTcwADEzNTY1NzQzNjAxADAxMTczMQAgMAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAB1c3RhciAgAHJvb3QAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAcm9vdAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA8IURPQ1RZUEUg
aHRtbD4KPGh0bWw+CiAgICA8aGVhZD4KICAgICAgICA8bWV0YSBjaGFyc2V0PSJVVEYtOCI+CiAg
ICAgICAgPHRpdGxlPlZlZWFtIFBOIGlzIGluaXRpYWxpemluZzwvdGl0bGU+CiAgICAgICAgPHN0
eWxlPgogICAgICAgICAgICBib2R5IHsKICAgICAgICAgICAgICAgIGZvbnQtZmFtaWx5OiBzYW5z
LXNlcmlmOwogICAgICAgICAgICB9CiAgICAgICAgICAgIHNwYW4gewogICAgICAgICAgICAgICAg
Zm9udC1mYW1pbHk6IGluaGVyaXQ7CiAgICAgICAgICAgIH0KICAgICAgICAgICAgaDIgewogICAg
ICAgICAgICAgICAgdGV4dC1hbGlnbjogY2VudGVyOwogICAgICAgICAgICAgICAgY29sb3I6IGdy
YXk7CiAgICAgICAgICAgIH0KICAgICAgICAgICAgI2NvbnRlbnQgewogICAgICAgICAgICAgICAg
dG9wOiA1MCU7CiAgICAgICAgICAgICAgICBsZWZ0OiA1MCU7CiAgICAgICAgICAgICAgICB3aWR0
aDogMTAwJTsKICAgICAgICAgICAgICAgIHBvc2l0aW9uOiBmaXhlZDsKICAgICAgICAgICAgICAg
IHRyYW5zZm9ybTogdHJhbnNsYXRlKC01MCUsIC01MCUpOwogICAgICAgICAgICB9CiAgICAgICAg
ICAgICNzdGF0dXMgewogICAgICAgICAgICAgICAgbWFyZ2luOiBhdXRvOwogICAgICAgICAgICAg
ICAgd2lkdGg6IDUwZW07CiAgICAgICAgICAgICAgICBoZWlnaHQ6IDIwZW07CiAgICAgICAgICAg
ICAgICBwYWRkaW5nOiAwLjVlbTsKICAgICAgICAgICAgICAgIG92ZXJmbG93OiBhdXRvOwogICAg
ICAgICAgICAgICAgYm9yZGVyOiAxcHggc29saWQgZ3JheTsKICAgICAgICAgICAgICAgIGZvbnQt
ZmFtaWx5OiBtb25vc3BhY2U7CiAgICAgICAgICAgIH0KICAgICAgICAgICAgI3N0dWIgewogICAg
ICAgICAgICAgICAgd2lkdGg6IDEwMCU7CiAgICAgICAgICAgICAgICBkaXNwbGF5OiBibG9jazsK
ICAgICAgICAgICAgICAgIGxpbmUtaGVpZ2h0OiAyMGVtOwogICAgICAgICAgICAgICAgdGV4dC1h
bGlnbjogY2VudGVyOwogICAgICAgICAgICAgICAgY29sb3I6IGRhcmtncmF5OwogICAgICAgICAg
ICB9CiAgICAgICAgICAgICNyZXN1bHQgewogICAgICAgICAgICAgICAgbWFyZ2luOiBhdXRvOwog
ICAgICAgICAgICAgICAgaGVpZ2h0OiAzZW07CiAgICAgICAgICAgICAgICBwYWRkaW5nOiAyZW07
CiAgICAgICAgICAgICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7CiAgICAgICAgICAgIH0KICAgICAg
ICAgICAgI29wZW4gewogICAgICAgICAgICAgICAgYm9yZGVyLXdpZHRoOiAxcHg7CiAgICAgICAg
ICAgICAgICBib3JkZXItc3R5bGU6IHNvbGlkOwogICAgICAgICAgICAgICAgYmFja2dyb3VuZC1j
b2xvcjogbGlnaHRzbGF0ZWdyYXk7CiAgICAgICAgICAgICAgICB0ZXh0LWRlY29yYXRpb246IG5v
bmU7CiAgICAgICAgICAgICAgICBwYWRkaW5nOiA4cHggMTBweDsKICAgICAgICAgICAgICAgIGNv
bG9yOiB3aGl0ZTsKICAgICAgICAgICAgfQogICAgICAgICAgICAjb3Blbjpob3ZlciB7CiAgICAg
ICAgICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOiBzbGF0ZWdyYXk7CiAgICAgICAgICAgIH0KICAg
ICAgICAgICAgLmhpZGRlbiB7CiAgICAgICAgICAgICAgICBkaXNwbGF5OiBub25lOwogICAgICAg
ICAgICB9CiAgICAgICAgICAgIC50aW1lIHsKICAgICAgICAgICAgICAgIGNvbG9yOiBncmF5Owog
ICAgICAgICAgICB9CiAgICAgICAgICAgIC5pbmZvLAogICAgICAgICAgICAubGFzdCB7CiAgICAg
ICAgICAgICAgICBjb2xvcjogYmxhY2s7CiAgICAgICAgICAgICAgICBtYXJnaW4tbGVmdDogMS41
ZW07CiAgICAgICAgICAgIH0KICAgICAgICAgICAgLmZhaWwgewogICAgICAgICAgICAgICAgY29s
b3I6IHJlZDsKICAgICAgICAgICAgICAgIG1hcmdpbi1sZWZ0OiAxLjVlbTsKICAgICAgICAgICAg
fQogICAgICAgICAgICAuZmFpbDo6YmVmb3JlIHsKICAgICAgICAgICAgICAgIGNvbnRlbnQ6ICdc
MjZENFxGRTBGJzsKICAgICAgICAgICAgICAgIHRleHQtYWxpZ246IGNlbnRlcjsKICAgICAgICAg
ICAgICAgIGRpc3BsYXk6IGlubGluZS1ibG9jazsKICAgICAgICAgICAgICAgIG1hcmdpbi1sZWZ0
OiAtMS41ZW07CiAgICAgICAgICAgICAgICB3aWR0aDogMS41ZW07CiAgICAgICAgICAgIH0KICAg
ICAgICAgICAgLndhcm4gewogICAgICAgICAgICAgICAgY29sb3I6IGRhcmtvcmFuZ2U7CiAgICAg
ICAgICAgICAgICBtYXJnaW4tbGVmdDogMS41ZW07CiAgICAgICAgICAgIH0KICAgICAgICAgICAg
Lndhcm46OmJlZm9yZSB7CiAgICAgICAgICAgICAgICBjb250ZW50OiAnXDI2QTBcRkUwRic7CiAg
ICAgICAgICAgICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7CiAgICAgICAgICAgICAgICBkaXNwbGF5
OiBpbmxpbmUtYmxvY2s7CiAgICAgICAgICAgICAgICBtYXJnaW4tbGVmdDogLTEuNWVtOwogICAg
ICAgICAgICAgICAgd2lkdGg6IDEuNWVtOwogICAgICAgICAgICB9CiAgICAgICAgICAgIC5pbmZv
Omxhc3Qtb2YtdHlwZTo6YmVmb3JlLAogICAgICAgICAgICAud2FybjpsYXN0LW9mLXR5cGU6OmJl
Zm9yZSB7CiAgICAgICAgICAgICAgICBjb250ZW50OnVybCgncnVubmluZy5naWYnKTsKICAgICAg
ICAgICAgICAgIHRleHQtYWxpZ246IGNlbnRlcjsKICAgICAgICAgICAgICAgIHZlcnRpY2FsLWFs
aWduOiBtaWRkbGU7CiAgICAgICAgICAgICAgICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7CiAgICAg
ICAgICAgICAgICBtYXJnaW4tbGVmdDogLTEuNWVtOwogICAgICAgICAgICAgICAgd2lkdGg6IDEu
NWVtOwogICAgICAgICAgICB9CiAgICAgICAgPC9zdHlsZT4KICAgICAgICA8c2NyaXB0IHR5cGU9
InRleHQvamF2YXNjcmlwdCI+CiAgICAgICAgICAgICd1c2Ugc3RyaWN0JzsKCiAgICAgICAgICAg
IHZhciByZXF1ZXN0ID0gbmV3IFhNTEh0dHBSZXF1ZXN0KCk7CgogICAgICAgICAgICB2YXIgU0hP
V19PUEVOX1RJTUVPVVQgPSA0MDAwOwogICAgICAgICAgICB2YXIgVVBEQVRFX0xPR19USU1FT1VU
ID0gMjAwMDsKICAgICAgICAgICAgdmFyIFJFTE9BRF9QQUdFX1RJTUVPVVQgPSA0MDAwOwoKICAg
ICAgICAgICAgdmFyIGxvZ1RpbWUgPSAwOwogICAgICAgICAgICB2YXIgbG9nVGV4dCA9ICcnOwog
ICAgICAgICAgICB2YXIgbG9nRmFpbCA9IGZhbHNlOwogICAgICAgICAgICB2YXIgbG9nTGFzdCA9
IGZhbHNlOwoKICAgICAgICAgICAgZnVuY3Rpb24gdmFsaWQobGluZSkgewogICAgICAgICAgICAg
ICAgcmV0dXJuIGxpbmUubGVuZ3RoICE9PSAwOwogICAgICAgICAgICB9CgogICAgICAgICAgICBm
dW5jdGlvbiBwYXJzZShsaW5lKSB7CiAgICAgICAgICAgICAgICByZXR1cm4gSlNPTi5wYXJzZShs
aW5lKTsKICAgICAgICAgICAgfQoKICAgICAgICAgICAgZnVuY3Rpb24gZnJlc2goZXZlbnQpIHsK
ICAgICAgICAgICAgICAgIHJldHVybiBldmVudC50aW1lID4gbG9nVGltZTsKICAgICAgICAgICAg
fQoKICAgICAgICAgICAgZnVuY3Rpb24gaXNGYWlsKGV2ZW50KSB7CiAgICAgICAgICAgICAgICBy
ZXR1cm4gZXZlbnQudHlwZSA9PT0gJ2ZhaWwnOwogICAgICAgICAgICB9CgogICAgICAgICAgICBm
dW5jdGlvbiBpc0xhc3QoZXZlbnQpIHsKICAgICAgICAgICAgICAgIHJldHVybiBldmVudC50eXBl
ID09PSAnbGFzdCc7CiAgICAgICAgICAgIH0KCiAgICAgICAgICAgIGZ1bmN0aW9uIGdldFRpbWUo
ZXZlbnQpIHsKICAgICAgICAgICAgICAgIHZhciBkYXRlID0gbmV3IERhdGUoZXZlbnQudGltZSk7
CgogICAgICAgICAgICAgICAgZnVuY3Rpb24gcGFkKG4pIHsKICAgICAgICAgICAgICAgICAgICBy
ZXR1cm4gbiA8IDEwID8gJzAnICsgbiA6IG47CiAgICAgICAgICAgICAgICB9CgogICAgICAgICAg
ICAgICAgcmV0dXJuIHBhZChkYXRlLmdldEhvdXJzKCkpCiAgICAgICAgICAgICAgICAgICAgICsg
JzonCiAgICAgICAgICAgICAgICAgICAgICsgcGFkKGRhdGUuZ2V0TWludXRlcygpKQogICAgICAg
ICAgICAgICAgICAgICArICc6JwogICAgICAgICAgICAgICAgICAgICArIHBhZChkYXRlLmdldFNl
Y29uZHMoKSk7CiAgICAgICAgICAgIH0KCiAgICAgICAgICAgIGZ1bmN0aW9uIGFwcGVuZChldmVu
dCkgewogICAgICAgICAgICAgICAgdmFyIHRpbWUgPSBkb2N1bWVudC5jcmVhdGVFbGVtZW50KCdz
cGFuJyk7CiAgICAgICAgICAgICAgICB2YXIgdGV4dCA9IGRvY3VtZW50LmNyZWF0ZUVsZW1lbnQo
J3NwYW4nKTsKICAgICAgICAgICAgICAgIHZhciBlbmRsID0gZG9jdW1lbnQuY3JlYXRlRWxlbWVu
dCgnYnInKTsKCiAgICAgICAgICAgICAgICB0aW1lLnRleHRDb250ZW50ID0gZ2V0VGltZShldmVu
dCk7CiAgICAgICAgICAgICAgICB0ZXh0LnRleHRDb250ZW50ID0gZXZlbnQudGV4dDsKCiAgICAg
ICAgICAgICAgICB0ZXh0LmNsYXNzTGlzdC5hZGQoZXZlbnQudHlwZSk7CiAgICAgICAgICAgICAg
ICB0aW1lLmNsYXNzTGlzdC5hZGQoJ3RpbWUnKTsKCiAgICAgICAgICAgICAgICB0aGlzLmFwcGVu
ZENoaWxkKHRpbWUpOwogICAgICAgICAgICAgICAgdGhpcy5hcHBlbmRDaGlsZCh0ZXh0KTsKICAg
ICAgICAgICAgICAgIHRoaXMuYXBwZW5kQ2hpbGQoZW5kbCk7CgogICAgICAgICAgICAgICAgbG9n
VGltZSA9IGV2ZW50LnRpbWU7CgogICAgICAgICAgICAgICAgaWYgKGlzRmFpbChldmVudCkpIHsK
ICAgICAgICAgICAgICAgICAgICBsb2dGYWlsID0gdHJ1ZTsKICAgICAgICAgICAgICAgIH0KCiAg
ICAgICAgICAgICAgICBpZiAoaXNMYXN0KGV2ZW50KSkgewogICAgICAgICAgICAgICAgICAgIGxv
Z0xhc3QgPSB0cnVlOwogICAgICAgICAgICAgICAgfQogICAgICAgICAgICB9CgogICAgICAgICAg
ICBmdW5jdGlvbiBzaG93KCkgewogICAgICAgICAgICAgICAgdGhpcy5jbGFzc0xpc3QucmVtb3Zl
KCdoaWRkZW4nKTsKICAgICAgICAgICAgfQoKICAgICAgICAgICAgZnVuY3Rpb24gcmVsb2FkUGFn
ZSgpIHsKICAgICAgICAgICAgICAgIHdpbmRvdy5sb2NhdGlvbi5yZWxvYWQodHJ1ZSk7CiAgICAg
ICAgICAgIH0KCiAgICAgICAgICAgIGZ1bmN0aW9uIHByb2Nlc3NMb2codGV4dCkgewogICAgICAg
ICAgICAgICAgdmFyIGxpbmVzID0gdGV4dC5zcGxpdCgnXG4nKS5maWx0ZXIodmFsaWQpOwogICAg
ICAgICAgICAgICAgdmFyIGV2ZW50cyA9IGxpbmVzLm1hcChwYXJzZSkuZmlsdGVyKGZyZXNoKTsK
CiAgICAgICAgICAgICAgICBpZiAoZXZlbnRzLmxlbmd0aCAhPT0gMCkgewogICAgICAgICAgICAg
ICAgICAgIHZhciBmcmFnbWVudCA9IGRvY3VtZW50LmNyZWF0ZURvY3VtZW50RnJhZ21lbnQoKTsK
ICAgICAgICAgICAgICAgICAgICB2YXIgc3RhdHVzID0gZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQo
J3N0YXR1cycpOwogICAgICAgICAgICAgICAgICAgIHZhciBzdHViID0gZG9jdW1lbnQuZ2V0RWxl
bWVudEJ5SWQoJ3N0dWInKTsKCiAgICAgICAgICAgICAgICAgICAgZXZlbnRzLmZvckVhY2goYXBw
ZW5kLmJpbmQoZnJhZ21lbnQpKTsKCiAgICAgICAgICAgICAgICAgICAgaWYgKHN0dWIgPT09IG51
bGwpIHsKICAgICAgICAgICAgICAgICAgICAgICAgc3RhdHVzLmFwcGVuZENoaWxkKGZyYWdtZW50
KTsKICAgICAgICAgICAgICAgICAgICB9CiAgICAgICAgICAgICAgICAgICAgZWxzZSB7CiAgICAg
ICAgICAgICAgICAgICAgICAgIHN0YXR1cy5yZXBsYWNlQ2hpbGQoZnJhZ21lbnQsIHN0dWIpOwog
ICAgICAgICAgICAgICAgICAgIH0KCiAgICAgICAgICAgICAgICAgICAgdmFyIGxhc3QgPSBzdGF0
dXMubGFzdEVsZW1lbnRDaGlsZDsKICAgICAgICAgICAgICAgICAgICBsYXN0LnNjcm9sbEludG9W
aWV3KHtiZWhhdmlvcjogJ3Ntb290aCd9KTsKICAgICAgICAgICAgICAgIH0KCiAgICAgICAgICAg
ICAgICBpZiAobG9nRmFpbCA9PT0gZmFsc2UpIHsKICAgICAgICAgICAgICAgICAgICBpZiAobG9n
TGFzdCA9PT0gZmFsc2UpIHsKICAgICAgICAgICAgICAgICAgICAgICAgc2V0VGltZW91dCh1cGRh
dGVMb2csIFVQREFURV9MT0dfVElNRU9VVCk7CiAgICAgICAgICAgICAgICAgICAgfQogICAgICAg
ICAgICAgICAgICAgIGVsc2UgewogICAgICAgICAgICAgICAgICAgICAgICB2YXIgb3BlbiA9IGRv
Y3VtZW50LmdldEVsZW1lbnRCeUlkKCdvcGVuJyk7CiAgICAgICAgICAgICAgICAgICAgICAgIHNl
dFRpbWVvdXQoc2hvdy5iaW5kKG9wZW4pLCBTSE9XX09QRU5fVElNRU9VVCk7CiAgICAgICAgICAg
ICAgICAgICAgfQogICAgICAgICAgICAgICAgfQogICAgICAgICAgICB9CgogICAgICAgICAgICBm
dW5jdGlvbiBwcm9jZXNzRXJyb3IoKSB7CiAgICAgICAgICAgICAgICBzZXRUaW1lb3V0KHJlbG9h
ZFBhZ2UsIFJFTE9BRF9QQUdFX1RJTUVPVVQpOwogICAgICAgICAgICB9CgogICAgICAgICAgICBm
dW5jdGlvbiBwcm9jZXNzRGF0YSgpIHsKICAgICAgICAgICAgICAgIGlmICh0aGlzLnN0YXR1cyA9
PT0gMjAwKSB7CiAgICAgICAgICAgICAgICAgICAgaWYgKGxvZ1RleHQubGVuZ3RoICE9PSB0aGlz
LnJlc3BvbnNlVGV4dC5sZW5ndGgpIHsKICAgICAgICAgICAgICAgICAgICAgICAgcHJvY2Vzc0xv
Zyhsb2dUZXh0ID0gdGhpcy5yZXNwb25zZVRleHQpOwogICAgICAgICAgICAgICAgICAgIH0KICAg
ICAgICAgICAgICAgICAgICBlbHNlIHsKICAgICAgICAgICAgICAgICAgICAgICAgc2V0VGltZW91
dCh1cGRhdGVMb2csIFVQREFURV9MT0dfVElNRU9VVCk7CiAgICAgICAgICAgICAgICAgICAgfQog
ICAgICAgICAgICAgICAgfQogICAgICAgICAgICAgICAgZWxzZSB7CiAgICAgICAgICAgICAgICAg
ICAgc2V0VGltZW91dChyZWxvYWRQYWdlLCBSRUxPQURfUEFHRV9USU1FT1VUKTsKICAgICAgICAg
ICAgICAgIH0KICAgICAgICAgICAgfQoKICAgICAgICAgICAgZnVuY3Rpb24gdXBkYXRlTG9nKCkg
ewogICAgICAgICAgICAgICAgdmFyIGZpbGUgPSAnd2ViLmxvZy50eHQ/JyArIERhdGUubm93KCk7
CiAgICAgICAgICAgICAgICByZXF1ZXN0Lm9wZW4oJ0dFVCcsIGZpbGUsIHRydWUpOwogICAgICAg
ICAgICAgICAgcmVxdWVzdC5vbmVycm9yID0gcHJvY2Vzc0Vycm9yOwogICAgICAgICAgICAgICAg
cmVxdWVzdC5vbmxvYWQgPSBwcm9jZXNzRGF0YTsKICAgICAgICAgICAgICAgIHJlcXVlc3Quc2Vu
ZChudWxsKTsKICAgICAgICAgICAgfQogICAgICAgIDwvc2NyaXB0PgogICAgPC9oZWFkPgogICAg
PGJvZHkgb25sb2FkPSJ1cGRhdGVMb2coKSI+CiAgICAgICAgPGRpdiBpZD0iY29udGVudCI+CiAg
ICAgICAgICAgIDxoMj5WZWVhbSBQTiBpcyBpbml0aWFsaXppbmc8L2gyPgogICAgICAgICAgICA8
ZGl2IGlkPSJzdGF0dXMiPgogICAgICAgICAgICAgICAgPHNwYW4gaWQ9InN0dWIiPlBsZWFzZSB3
YWl0LCBpdCB3aWxsIGJlIGF2YWlsYWJsZSBzb29uPC9zcGFuPgogICAgICAgICAgICA8L2Rpdj4K
ICAgICAgICAgICAgPGRpdiBpZD0icmVzdWx0Ij4KICAgICAgICAgICAgICAgIDxhIGlkPSJvcGVu
IiBjbGFzcz0iaGlkZGVuIiBocmVmPSJqYXZhc2NyaXB0OnJlbG9hZFBhZ2UoKSI+TG9naW4gdG8g
VmVlYW0gUE48L2E+CiAgICAgICAgICAgIDwvZGl2PgogICAgICAgIDwvZGl2PgogICAgPC9ib2R5
Pgo8L2h0bWw+CgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAuL3N0YXJ0dXAuY2ZnAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAMDAwMDc1NQAwMDAwMDAwADAwMDAwMDAAMDAwMDAwMDEzMjAAMTM1
NjU3NDM2MDEAMDEyMTA1ACAwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAHVzdGFyICAAcm9vdAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAByb290AAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACMhL3Vzci9iaW4vZW52IGJhc2gKCmFza19j
b25maXJtYXRpb249dHJ1ZQppZ25vcmVfcHJlY2hlY2tzPWZhbHNlCmNvbmZpZ3VyZV9zeXN0ZW09
ZmFsc2UKYXp1cmVfZGVwbG95bWVudD1mYWxzZQphd3NfZGVwbG95bWVudD1mYWxzZQoKcmVhZG9u
bHkgaW5zdGFsbF9maWxlbmFtZT0iVmVlYW1QTi1pbnN0YWxsZXIucnVuLnNoIgoKcmVhZG9ubHkg
cHJvZHVjdF9uYW1lPSJWZWVhbSBQTiAoUG93ZXJlZCBOZXR3b3JrKSIKcmVhZG9ubHkgcHJvZHVj
dF9saWNlbnNlPSJodHRwczovL3d3dy52ZWVhbS5jb20vZXVsYS5odG1sIgpyZWFkb25seSBwcm9k
dWN0X2NvcHlyaWdodD0iwqkgMjAxOSBWZWVhbSBTb2Z0d2FyZSBHcm91cCBHbWJIIgoKcmVhZG9u
bHkgYXB0X3JlcG9zaXRvcnlfa2V5PSJodHRwOi8vcmVwb3NpdG9yeS52ZWVhbS5jb20va2V5cy92
ZWVhbS5ncGciCnJlYWRvbmx5IGFwdF9yZXBvc2l0b3J5X3NyYz0iZGViIFthcmNoPWFtZDY0XSBo
dHRwOi8vcmVwb3NpdG9yeS52ZWVhbS5jb20vcG4vcHVibGljIHBuIHN0YWJsZSIKCnJlYWRvbmx5
IGZhdmljb25fZmlsZT0iZmF2aWNvbi5pY28iCnJlYWRvbmx5IHJ1bm5pbmdfaW1hZ2U9InJ1bm5p
bmcuZ2lmIgpyZWFkb25seSBpbmRleF9odG1sX3BhZ2U9ImluZGV4Lmh0bWwiCgpyZWFkb25seSB3
ZWJfbG9nX3NjcmlwdD0id2ViX2xvZy5zaCIKcmVhZG9ubHkgd2ViX3V0aWxzX3NjcmlwdD0id2Vi
X3V0aWxzLnNoIgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAuL3N0YXJ0dXAuc2gAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAMDAwMDc1NQAwMDAwMDAwADAwMDAwMDAAMDAwMDAwMzAwMjMAMTM1NjU3
NDM2MDEAMDExNzYyACAwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AHVzdGFyICAAcm9vdAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAByb290AAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACMhL3Vzci9iaW4vZW52IGJhc2gKCiNzaGVsbGNo
ZWNrIGRpc2FibGU9U0MxMDkwLFNDMTA5MSxTQzIxMjAsU0MyMTU0LFNDMjE1NSxTQzIxODEKCmZ1
bmN0aW9uIHdlYl9sb2dfd2FybmluZyB7IHRydWU7IH0KZnVuY3Rpb24gd2ViX2xvZ19mYWlsIHsg
dHJ1ZTsgfQpmdW5jdGlvbiB3ZWJfbG9nIHsgdHJ1ZTsgfQoKZnVuY3Rpb24gd2FybmluZwp7CiAg
ICB3ZWJfbG9nX3dhcm5pbmcgIiQqIgogICAgZWNobyAiJCoiID4mMgp9CgpmdW5jdGlvbiBlcnJv
cgp7CiAgICB3ZWJfbG9nX2ZhaWwgIiQqIgogICAgZWNobyAiJCoiID4mMgp9CgpmdW5jdGlvbiBs
YXN0CnsKICAgIHdlYl9sb2dfbGFzdCAiJCoiCiAgICBlY2hvICIkKiIKfQoKZnVuY3Rpb24gZmFp
bAp7CiAgICBlcnJvciAiJCoiCiAgICBleGl0IDEKfQoKZnVuY3Rpb24gbG9nCnsKICAgIHdlYl9s
b2cgIiQqIgogICAgZWNobyAiJCoiCn0KCmZ1bmN0aW9uIGdldF9kaXN0cm9faWQKewogICAgKHNv
dXJjZSAvZXRjL29zLXJlbGVhc2U7IGVjaG8gIiR7SUR9XyR7VkVSU0lPTl9JRH0iKQp9CgpmdW5j
dGlvbiBjaGVja19vc192ZXJzaW9uCnsKICAgIGlmIFtbICQodW5hbWUpICE9ICdMaW51eCcgXV07
IHRoZW4KICAgICAgICBmYWlsICdUaGlzIHNjcmlwdCBjYW4gb25seSBiZSBydW4gb24gTGludXgn
CiAgICBmaQoKICAgIGlmIFtbICQoZ2V0X2Rpc3Ryb19pZCkgIT0gJ3VidW50dV8xOC4wNCcgXV07
IHRoZW4KICAgICAgICBmYWlsICdUaGlzIHNjcmlwdCBjYW4gb25seSBiZSBydW4gb24gVWJ1bnR1
IDE4LjA0JwogICAgZmkKfQoKY2hlY2tfb3NfdmVyc2lvbgoKcmVhZG9ubHkgc3RhcnR1cF9zY3Jp
cHQ9JChyZWFscGF0aCAiJDAiKQpyZWFkb25seSBzdGFydHVwX2NvbmZpZz0ke3N0YXJ0dXBfc2Ny
aXB0Ly5zaC8uY2ZnfQoKaWYgW1sgISAtZiAkc3RhcnR1cF9jb25maWcgXV07IHRoZW4KICAgIGZh
aWwgJ0Nhbm5vdCBmaW5kIGluc3RhbGxlciBjb25maWd1cmF0aW9uJwpmaQoKc291cmNlICIkc3Rh
cnR1cF9jb25maWciCgpzb3VyY2UgIiR3ZWJfbG9nX3NjcmlwdCIKc291cmNlICIkd2ViX3V0aWxz
X3NjcmlwdCIKCgpmdW5jdGlvbiBzaG93X3ZlcnNpb25faW5mbwp7CiAgICBlY2hvICIke3Byb2R1
Y3RfbmFtZX0iCiAgICBlY2hvICJDb3B5cmlnaHQgJHtwcm9kdWN0X2NvcHlyaWdodH0iCiAgICBl
Y2hvICJMaWNlbnNlOiAke3Byb2R1Y3RfbGljZW5zZX0iCn0KCmZ1bmN0aW9uIHNob3dfaGVscF9t
ZXNzYWdlCnsKICAgIGNhdCA8PCBFT0YKVXNhZ2U6ICR7aW5zdGFsbF9maWxlbmFtZX0gLS0gW29w
dGlvbnNdCk9wdGlvbnMKICAtYywgLS1jb25maWd1cmUtc3lzdGVtICAgIHByZS1jb25maWd1cmUg
c3lzdGVtIGJlZm9yZSBpbnN0YWxsYXRpb24KICAtZiwgLS1mb3JjZSAgICAgICAgICAgICAgIGZv
cmNlIGluc3RhbGxhdGlvbiAoaWdub3JlIHByZS1jaGVja3MpCiAgLXksIC0tcXVpZXQgICAgICAg
ICAgICAgICBxdWlldCBtb2RlICh1bmF0dGVuZGVkIGluc3RhbGxhdGlvbikKICAtdiwgLS12ZXJz
aW9uICAgICAgICAgICAgIHByaW50IHZlcnNpb24gaW5mb3JtYXRpb24KICAtaCwgLS1oZWxwICAg
ICAgICAgICAgICAgIHByaW50IHRoaXMgaGVscCBtZXNzYWdlCkVPRgp9Cgp3aGlsZSB0cnVlOyBk
bwogICAgY2FzZSAkMSBpbgogICAgICAgIC1jfC0tY29uZmlndXJlLXN5c3RlbSApCiAgICAgICAg
ICAgIGNvbmZpZ3VyZV9zeXN0ZW09dHJ1ZQogICAgICAgICAgICBzaGlmdAogICAgICAgICAgICA7
OwogICAgICAgIC1mfC0tZm9yY2UgKQogICAgICAgICAgICBpZ25vcmVfcHJlY2hlY2tzPXRydWUK
ICAgICAgICAgICAgc2hpZnQKICAgICAgICAgICAgOzsKICAgICAgICAteXwtLXF1aWV0ICkKICAg
ICAgICAgICAgYXNrX2NvbmZpcm1hdGlvbj1mYWxzZQogICAgICAgICAgICBzaGlmdAogICAgICAg
ICAgICA7OwogICAgICAgIC12fC0tdmVyc2lvbiApCiAgICAgICAgICAgIHNob3dfdmVyc2lvbl9p
bmZvCiAgICAgICAgICAgIGV4aXQgMAogICAgICAgICAgICA7OwogICAgICAgIC1ofC0taGVscCAp
CiAgICAgICAgICAgIHNob3dfaGVscF9tZXNzYWdlCiAgICAgICAgICAgIGV4aXQgMAogICAgICAg
ICAgICA7OwogICAgICAgIC0qICkKICAgICAgICAgICAgZXJyb3IgIiR7aW5zdGFsbF9maWxlbmFt
ZX06IGludmFsaWQgb3B0aW9uOiAkMSIKICAgICAgICAgICAgc2hvd19oZWxwX21lc3NhZ2UKICAg
ICAgICAgICAgZXhpdCAxCiAgICAgICAgICAgIDs7CiAgICAgICAgKiApCiAgICAgICAgICAgIGJy
ZWFrCiAgICAgICAgICAgIDs7CiAgICBlc2FjCmRvbmUKCgppZiBbWyAkRVVJRCAtbmUgMCBdXTsg
dGhlbgogICAgZmFpbCAnVGhpcyBzY3JpcHQgbXVzdCBiZSBydW4gYXMgcm9vdCcKZmkKCmZ1bmN0
aW9uIGFwdF9sb2NrX3dhaXQKewogICAgbG9jYWwgbWF4X3JldHJpZXM9MTAKICAgIGxvY2FsIGxv
Y2tlZF9maWxlcz0wCgogICAgbG9jYWwgYXB0X2xvY2tfZmlsZXM9KCcvdmFyL2xpYi9hcHQvbGlz
dHMvbG9jaycgJy92YXIvbGliL2Rwa2cvbG9jaycgJy92YXIvbGliL2Rwa2cvbG9jay1mcm9udGVu
ZCcpCgogICAgZm9yICgocmV0cnk9MTsgcmV0cnk8PW1heF9yZXRyaWVzOyByZXRyeSsrKSk7IGRv
CiAgICAgICAgbG9ja2VkX2ZpbGVzPTAKCiAgICAgICAgZm9yIGFwdF9sb2NrX2ZpbGUgaW4gIiR7
YXB0X2xvY2tfZmlsZXNbQF19IjsgZG8KICAgICAgICAgICAgaWYgZnVzZXIgLXMgIiRhcHRfbG9j
a19maWxlIjsgdGhlbgogICAgICAgICAgICAgICAgKChsb2NrZWRfZmlsZXMrKykpCiAgICAgICAg
ICAgIGZpCiAgICAgICAgZG9uZQoKICAgICAgICBpZiAoKGxvY2tlZF9maWxlcz4wKSk7IHRoZW4K
ICAgICAgICAgICAgd2FybmluZyAiUGFja2FnZSBtYW5hZ2VyIGRhdGFiYXNlIGlzIGxvY2tlZCAo
cmV0cnkgJHtyZXRyeX0gb2YgJHttYXhfcmV0cmllc30pIgogICAgICAgIGVsc2UKICAgICAgICAg
ICAgYnJlYWsKICAgICAgICBmaQoKICAgICAgICBzbGVlcCA2MAogICAgZG9uZQoKICAgIGlmICgo
bG9ja2VkX2ZpbGVzPjApKTsgdGhlbgogICAgICAgIGZhaWwgIlBhY2thZ2UgbWFuYWdlciBkYXRh
YmFzZSBpcyBzdGlsbCBsb2NrZWQgYWZ0ZXIgJHttYXhfcmV0cmllc30gcmV0cmllcyIKICAgIGZp
Cn0KCmZ1bmN0aW9uIGFwdF9hZGRfcmVwb3NpdG9yeQp7CiAgICBhcHQtYWRkLXJlcG9zaXRvcnkg
LS15ZXMgLS1uby11cGRhdGUgIiRAIgp9CgpmdW5jdGlvbiBhcHRfZ2V0CnsKICAgIGFwdF9sb2Nr
X3dhaXQgJiYgYXB0LWdldCAtcXEgLW89J0Rwa2c6OlVzZS1QdHk9MCcgIiRAIgp9CgpmdW5jdGlv
biBhcHRfa2V5CnsKICAgIGFwdF9sb2NrX3dhaXQgJiYgYXB0LWtleSAtLXF1aWV0ICIkQCIKfQoK
ZnVuY3Rpb24gYXNrX2NvbmZpcm1hdGlvbgp7CiAgICB3aGlsZSB0cnVlOyBkbwogICAgICAgIHJl
YWQgLXIgLXAgIlRoaXMgc2NyaXB0IHdpbGwgaW5zdGFsbCAke3Byb2R1Y3RfbmFtZX0uIENvbnRp
bnVlPyBbWS9uXSAiIGFuc3dlcgoKICAgICAgICBjYXNlICRhbnN3ZXIgaW4KICAgICAgICAgICAg
TnxuICkgZmFpbCAnSW5zdGFsbGF0aW9uIGFib3J0ZWQnOzsKICAgICAgICAgICAgWXx5ICkgYnJl
YWs7OwogICAgICAgIGVzYWMKCiAgICAgICAgaWYgW1sgLXogJGFuc3dlciBdXTsgdGhlbgogICAg
ICAgICAgICBicmVhawogICAgICAgIGZpCiAgICBkb25lCn0KCmZ1bmN0aW9uIHBlcmZvcm1fcHJl
X2NoZWNrcwp7CiAgICBsb2cgJ1J1bm5pbmcgaW5zdGFsbGF0aW9uIHByZS1jaGVja3MnCgogICAg
ZnVuY3Rpb24gcHJlY2hlY2tfZmFpbGVkCiAgICB7CiAgICAgICAgd2FybmluZyAiUHJlLWNoZWNr
IGZhaWxlZDogJCoiCiAgICAgICAgKChmYWlsZWRfcHJlY2hlY2tzKyspKQogICAgfQoKICAgIGZ1
bmN0aW9uIGdldF9uZXR3b3JrX2FkYXB0ZXIKICAgIHsKICAgICAgICAvc2Jpbi9pcCAtbyAtNCBy
b3V0ZSBzaG93IHRvIGRlZmF1bHQgfCBncmVwIC1vUCAnZGV2IFxLXHcrJwogICAgfQoKICAgIGZ1
bmN0aW9uIGNoZWNrX3ZlZWFtcG5faW5zdGFsbGVkCiAgICB7CiAgICAgICAgaWYgW1sgLW4gJHZl
cnNpb24gXV07IHRoZW4KICAgICAgICAgICAgaWYgZHBrZyAtLWNvbXBhcmUtdmVyc2lvbnMgIiRp
bnN0YWxsZWRfdmVyc2lvbiIgbHQgJzIuMCc7IHRoZW4KICAgICAgICAgICAgICAgIHByZWNoZWNr
X2ZhaWxlZCAiQ29uZmxpY3RpbmcgdmVyc2lvbiAoJHtpbnN0YWxsZWRfdmVyc2lvbn0pIG9mIFZl
ZWFtIFBOIGlzIGluc3RhbGxlZC4gSXQgd2lsbCBiZSByZW1vdmVkIGJlZm9yZSBpbnN0YWxsYXRp
b24uIgogICAgICAgICAgICBmaQogICAgICAgIGZpCiAgICB9CgogICAgZnVuY3Rpb24gY2hlY2tf
bmdpbnhfY29uZmlndXJlZAogICAgewogICAgICAgIGlmIHN5c3RlbWN0bCAtcSBpcy1lbmFibGVk
IG5naW54IDI+L2Rldi9udWxsOyB0aGVuCiAgICAgICAgICAgIHByZWNoZWNrX2ZhaWxlZCAiTmdp
bnggd2ViIHNlcnZlciBpcyBpbnN0YWxsZWQgYW5kIGVuYWJsZWQuIFZlZWFtIFBOIG1heSBub3Qg
YmUgYWJsZSB0byBmdW5jdGlvbiBjb3JyZWN0bHkgYWZ0ZXIgaW5zdGFsbGF0aW9uLiIKICAgICAg
ICAgICAgZnVuY3Rpb24gY2hlY2tfd2ViX3NlcnZlcl9jb25maWd1cmVkIHsgdHJ1ZTsgfQogICAg
ICAgIGZpCiAgICB9CgogICAgZnVuY3Rpb24gY2hlY2tfYXBhY2hlX2NvbmZpZ3VyZWQKICAgIHsK
ICAgICAgICBpZiBzeXN0ZW1jdGwgLXEgaXMtZW5hYmxlZCBhcGFjaGUyIDI+L2Rldi9udWxsOyB0
aGVuCiAgICAgICAgICAgIHByZWNoZWNrX2ZhaWxlZCAiQXBhY2hlIHdlYiBzZXJ2ZXIgaXMgaW5z
dGFsbGVkIGFuZCBlbmFibGVkLiBFeGlzdGluZyBBcGFjaGUgY29uZmlndXJhdGlvbiB3aWxsIGJl
IG92ZXJ3cml0dGVuIGR1cmluZyBpbnN0YWxsYXRpb24uIgogICAgICAgICAgICBmdW5jdGlvbiBj
aGVja193ZWJfc2VydmVyX2NvbmZpZ3VyZWQgeyB0cnVlOyB9CiAgICAgICAgZmkKICAgIH0KCiAg
ICBmdW5jdGlvbiBjaGVja19kbnNtYXNxX2NvbmZpZ3VyZWQKICAgIHsKICAgICAgICBpZiBzeXN0
ZW1jdGwgLXEgaXMtZW5hYmxlZCBkbnNtYXNxIDI+L2Rldi9udWxsOyB0aGVuCiAgICAgICAgICAg
IHByZWNoZWNrX2ZhaWxlZCAiRG5zbWFzcSBpcyBpbnN0YWxsZWQgYW5kIGVuYWJsZWQuIEV4aXN0
aW5nIGRuc21hc3EgY29uZmlndXJhdGlvbiB3aWxsIGJlIG92ZXJ3cml0dGVuIGR1cmluZyBpbnN0
YWxsYXRpb24uIgogICAgICAgIGZpCiAgICB9CgogICAgZnVuY3Rpb24gY2hlY2tfbmV0cGxhbl9j
b25maWd1cmVkCiAgICB7CiAgICAgICAgaWYgW1sgISAteCAkKGNvbW1hbmQgLXYgbmV0cGxhbikg
XV07IHRoZW4KICAgICAgICAgICAgcHJlY2hlY2tfZmFpbGVkICJOZXRwbGFuIGlzIG5vdCBpbnN0
YWxsZWQuIE5ldHBsYW4gd2lsbCBiZSBpbnN0YWxsZWQgYW5kIHVzZWQgdG8gY29uZmlndXJlIG5l
dHdvcmtpbmcgZHVyaW5nIGluc3RhbGxhdGlvbi4iCiAgICAgICAgZmkKCiAgICAgICAgbG9jYWwg
YWRhcHRlcj0kKGdldF9uZXR3b3JrX2FkYXB0ZXIpCgogICAgICAgIGlmIFtbIC16ICQobmV0cGxh
biBnZW5lcmF0ZSAtLW1hcHBpbmcgIiRhZGFwdGVyIikgXV07IHRoZW4KICAgICAgICAgICAgcHJl
Y2hlY2tfZmFpbGVkICJOZXRwbGFuIGRvZXMgbm90IG1hbmFnZSBuZXR3b3JrIGNvbmZpZ3VyYXRp
b24uIE5ldHBsYW4gd2lsbCBiZSB1c2VkIHRvIGNvbmZpZ3VyZSBuZXR3b3JraW5nIGR1cmluZyBp
bnN0YWxsYXRpb24uIgogICAgICAgIGZpCiAgICB9CgogICAgZnVuY3Rpb24gY2hlY2tfb3BlbnZw
bl9jb25maWd1cmVkCiAgICB7CiAgICAgICAgaWYgW1sgLXggJChjb21tYW5kIC12IG9wZW52cG4p
ICYmIC1uICQocGlkb2Ygb3BlbnZwbikgXV07IHRoZW4KICAgICAgICAgICAgcHJlY2hlY2tfZmFp
bGVkICJPcGVuVlBOIGlzIGluc3RhbGxlZCBhbmQgcnVubmluZy4gRnVuY3Rpb25hbGl0eSBvZiBl
eGlzdGluZyBPcGVuVlBOIGNvbmZpZ3VyYXRpb25zIG1heWJlIGRpc3J1cHRlZCBhZnRlciBpbnN0
YWxsYXRpb24uIgogICAgICAgICAgICBmdW5jdGlvbiBjaGVja19vdGhlcl92cG5zX2NvbmZpZ3Vy
ZWQgeyB0cnVlOyB9CiAgICAgICAgZmkKICAgIH0KCiAgICBmdW5jdGlvbiBjaGVja193aXJlZ3Vh
cmRfY29uZmlndXJlZAogICAgewogICAgICAgIGlmIFtbIC14ICQoY29tbWFuZCAtdiB3ZykgJiYg
LW4gJCh3ZyBzaG93IGludGVyZmFjZXMpIF1dOyB0aGVuCiAgICAgICAgICAgIHByZWNoZWNrX2Zh
aWxlZCAiV2lyZUd1YXJkIGlzIGluc3RhbGxlZCBhbmQgY29uZmlndXJlZC4gRnVuY3Rpb25hbGl0
eSBvZiBleGlzdGluZyBXaXJlR3VhcmQgY29uZmlndXJhdGlvbnMgbWF5YmUgYmUgZGlzcnVwdGVk
IGFmdGVyIGluc3RhbGxhdGlvbi4iCiAgICAgICAgZmkKICAgIH0KCiAgICBmdW5jdGlvbiBjaGVj
a19vdGhlcl92cG5zX2NvbmZpZ3VyZWQKICAgIHsKICAgICAgICBpZiBbWyAtbiAkKGlwIHR1bnRh
cCBzaG93IDI+L2Rldi9udWxsKSBdXTsgdGhlbgogICAgICAgICAgICBwcmVjaGVja19mYWlsZWQg
IkZvdW5kIFRVTi9UQVAgaW50ZXJmYWNlcyhzKSBjb25maWd1cmVkLiBUaGlzIG1heSBpbmRpY2F0
ZSBhIFZQTiBzZXJ2aWNlIGlzIHJ1bm5pbmcuIEl0cyBmdW5jdGlvbmFsaXR5IG1heWJlIGRpc3J1
cHRlZCBhZnRlciBpbnN0YWxsYXRpb24uIgogICAgICAgIGZpCiAgICB9CgogICAgZnVuY3Rpb24g
Y2hlY2tfd2ViX3NlcnZlcl9jb25maWd1cmVkCiAgICB7CiAgICAgICAgaWYgW1sgLW4gJChsc29m
IC1pVENQOjgwIC1pVENQOjQ0MyAtc1RDUDpMSVNURU4pIF1dOyB0aGVuCiAgICAgICAgICAgIHBy
ZWNoZWNrX2ZhaWxlZCAiRm91bmQgVENQIHBvcnRzIDgwIGFuZCA0NDMgYXJlIGxpc3RlbmluZy4g
VGhpcyBtYXkgaW5kaWNhdGUgYSB3ZWIgc2VydmVyIGlzIHJ1bm5pbmcuIEl0cyBmdW5jdGlvbmFs
aXR5IG1heWJlIGRpc3J1cHRlZCBhZnRlciBpbnN0YWxsYXRpb24uIgogICAgICAgIGZpCiAgICB9
CgogICAgbG9jYWwgZmFpbGVkX3ByZWNoZWNrcz0wCgogICAgY2hlY2tfdmVlYW1wbl9pbnN0YWxs
ZWQKICAgIGNoZWNrX25naW54X2NvbmZpZ3VyZWQKICAgIGNoZWNrX2FwYWNoZV9jb25maWd1cmVk
CiAgICBjaGVja19uZXRwbGFuX2NvbmZpZ3VyZWQKICAgIGNoZWNrX2Ruc21hc3FfY29uZmlndXJl
ZAogICAgY2hlY2tfb3BlbnZwbl9jb25maWd1cmVkCiAgICBjaGVja193aXJlZ3VhcmRfY29uZmln
dXJlZAogICAgY2hlY2tfb3RoZXJfdnBuc19jb25maWd1cmVkCiAgICBjaGVja193ZWJfc2VydmVy
X2NvbmZpZ3VyZWQKCiAgICBpZiAoKGZhaWxlZF9wcmVjaGVja3M+MCkpOyB0aGVuCiAgICAgICAg
aWYgW1sgJGlnbm9yZV9wcmVjaGVja3MgPT0gdHJ1ZSBdXTsgdGhlbgogICAgICAgICAgICBsb2cg
J0luc3RhbGxhdGlvbiB3aWxsIGNvbnRpbnVlIChvcHRpb24gIi0tZm9yY2UiIHdhcyBzcGVjaWZp
ZWQpLicKICAgICAgICBlbHNlCiAgICAgICAgICAgIGZhaWwgJ0luc3RhbGxhdGlvbiBjYW5ub3Qg
Y29udGludWUgKG92ZXJyaWRlIHdpdGggIi0tZm9yY2UiIG9wdGlvbikuJwogICAgICAgIGZpCgog
ICAgICAgIHNsZWVwIDUKICAgIGZpCn0KCmZ1bmN0aW9uIHBlcmZvcm1fY2xlYW51cAp7CiAgICBs
YXN0ICdQZXJmb3JtaW5nIGNsZWFudXAnCgogICAgd2ViX2xvZ19maW5hbGl6ZQogICAgd2ViX3Nl
cnZlcl9zdG9wCgogICAgcm0gLWYgL3Vzci9zYmluL3BvbGljeS1yYy5kCgogICAgdHJhcCAtIEVY
SVQKfQoKdHJhcCBwZXJmb3JtX2NsZWFudXAgRVhJVAoKZnVuY3Rpb24gaW5zdGFsbF9wcmVyZXF1
aXNpdGVzCnsKICAgIGxvZyAnSW5zdGFsbGluZyBwcmVyZXF1aXNpdGUgcGFja2FnZXMnCgogICAg
ZWNobyBpcHRhYmxlcy1wZXJzaXN0ZW50IGlwdGFibGVzLXBlcnNpc3RlbnQvYXV0b3NhdmVfdjQg
Ym9vbGVhbiB0cnVlIHwgZGViY29uZi1zZXQtc2VsZWN0aW9ucwogICAgZWNobyBpcHRhYmxlcy1w
ZXJzaXN0ZW50IGlwdGFibGVzLXBlcnNpc3RlbnQvYXV0b3NhdmVfdjYgYm9vbGVhbiB0cnVlIHwg
ZGViY29uZi1zZXQtc2VsZWN0aW9ucwoKICAgIGV4cG9ydCBERUJJQU5fRlJPTlRFTkQ9bm9uaW50
ZXJhY3RpdmUKCiAgICBhcHRfZ2V0IGluc3RhbGwgc29mdHdhcmUtcHJvcGVydGllcy1jb21tb24g
bHNiLXJlbGVhc2UgY3VybCBnbnVwZyBuZXQtdG9vbHMKCiAgICBpZiBbWyAkPyAtbmUgMCBdXTsg
dGhlbgogICAgICAgIGZhaWwgJ0ZhaWxlZCB0byBpbnN0YWxsIHByZXJlcXVpc2l0ZSBwYWNrYWdl
cycKICAgIGZpCgogICAgbG9nICdBZGRpbmcgInVuaXZlcnNlIiByZXBvc2l0b3J5JwoKICAgIGFw
dF9hZGRfcmVwb3NpdG9yeSB1bml2ZXJzZQoKICAgIGlmIFtbICQ/IC1uZSAwIF1dOyB0aGVuCiAg
ICAgICAgZmFpbCAnRmFpbGVkIHRvIGFkZCAidW5pdmVyc2UiIHJlcG9zaXRvcnknCiAgICBmaQoK
ICAgIGFwdF9nZXQgdXBkYXRlCgogICAgaWYgW1sgJD8gLW5lIDAgXV07IHRoZW4KICAgICAgICB3
YXJuaW5nICdGYWlsZWQgdG8gdXBkYXRlIHBhY2thZ2UgZGF0YWJhc2UnCiAgICBmaQp9CgpmdW5j
dGlvbiBzdGFydF9pbml0X3dlYl9zaXRlCnsKICAgIGxvZyAnQ29uZmlndXJpbmcgaW5pdCB3ZWIg
c2l0ZScKCiAgICBsb2NhbCB2ZWVhbXBuX2luaXRfc2l0ZT0vdmFyL3d3dy92ZWVhbXBuLWluaXQK
CiAgICBpbnN0YWxsIC1tIDA3NTUgLW8gcm9vdCAtZyByb290IC1kICIkdmVlYW1wbl9pbml0X3Np
dGUiCgogICAgaW5zdGFsbCAtbSAwNjQ0IC1vIHJvb3QgLWcgcm9vdCAiJGZhdmljb25fZmlsZSIg
IiR2ZWVhbXBuX2luaXRfc2l0ZSIKICAgIGluc3RhbGwgLW0gMDY0NCAtbyByb290IC1nIHJvb3Qg
IiRydW5uaW5nX2ltYWdlIiAiJHZlZWFtcG5faW5pdF9zaXRlIgogICAgaW5zdGFsbCAtbSAwNjQ0
IC1vIHJvb3QgLWcgcm9vdCAiJGluZGV4X2h0bWxfcGFnZSIgIiR2ZWVhbXBuX2luaXRfc2l0ZSIK
CiAgICBpZiBbWyAtbiAkVkVFQU1QTl9XRUJfTE9HX0ZJTEUgXV07IHRoZW4KICAgICAgICBsbiAt
ZnMgLS10YXJnZXQtZGlyZWN0b3J5PSIkdmVlYW1wbl9pbml0X3NpdGUiICIkVkVFQU1QTl9XRUJf
TE9HX0ZJTEUiCiAgICBmaQoKICAgIGlmIHN5c3RlbWN0bCAtcSBpcy1hY3RpdmUgYXBhY2hlMiAy
Pi9kZXYvbnVsbDsgdGhlbgogICAgICAgIGxvZyAnU3RvcHBpbmcgQXBhY2hlIHdlYiBzZXJ2ZXIn
CiAgICAgICAgc3lzdGVtY3RsIHN0b3AgYXBhY2hlMgogICAgZmkKCiAgICBsb2cgJ1N0YXJ0aW5n
IEJ1c3lCb3ggd2ViIHNlcnZlcicKICAgIHdlYl9zZXJ2ZXJfc3RhcnQgIiR2ZWVhbXBuX2luaXRf
c2l0ZSIKfQoKZnVuY3Rpb24gc3RhcnRfbWFpbl93ZWJfc2l0ZQp7CiAgICBsb2cgJ1N0YXJ0aW5n
IFZlZWFtIFBOIHdlYiBVSScKCiAgICBpZiBhMnF1ZXJ5IC1xIC1zIHZlZWFtcG4taW5pdDsgdGhl
bgogICAgICAgIGEyZGlzc2l0ZSB2ZWVhbXBuLWluaXQKICAgIGZpCgogICAgaWYgISBhMnF1ZXJ5
IC1xIC1zIHZlZWFtcG4tc2l0ZTsgdGhlbgogICAgICAgIGEyZW5zc2l0ZSB2ZWVhbXBuLXNpdGUK
ICAgIGZpCgogICAgcGVyZm9ybV9jbGVhbnVwCgogICAgc3lzdGVtY3RsIHN0YXJ0IGFwYWNoZTIK
fQoKZnVuY3Rpb24gcHJlX2NvbmZpZ3VyZV9zeXN0ZW0KewogICAgbG9nICdQcmUtY29uZmlndXJp
bmcgc3lzdGVtJwoKICAgIHNlZCAtaSAncy8uKlVuYXR0ZW5kZWQtVXBncmFkZTo6QXV0b21hdGlj
LVJlYm9vdFxzLiovVW5hdHRlbmRlZC1VcGdyYWRlOjpBdXRvbWF0aWMtUmVib290ICJ0cnVlIjsv
JyAvZXRjL2FwdC9hcHQuY29uZi5kLzUwdW5hdHRlbmRlZC11cGdyYWRlcwogICAgc2VkIC1pICdz
Ly4qVW5hdHRlbmRlZC1VcGdyYWRlOjpBdXRvbWF0aWMtUmVib290LVRpbWVccy4qL1VuYXR0ZW5k
ZWQtVXBncmFkZTo6QXV0b21hdGljLVJlYm9vdC1UaW1lICIwMjowMCI7LycgL2V0Yy9hcHQvYXB0
LmNvbmYuZC81MHVuYXR0ZW5kZWQtdXBncmFkZXMKCiAgICBleHBvcnQgVUNGX0ZPUkNFX0NPTkZG
TkVXPVlFUwogICAgZXhwb3J0IERFQklBTl9GUk9OVEVORD1ub25pbnRlcmFjdGl2ZQoKICAgIGVj
aG8gdW5hdHRlbmRlZC11cGdyYWRlcyB1bmF0dGVuZGVkLXVwZ3JhZGVzL2VuYWJsZV9hdXRvX3Vw
ZGF0ZXMgYm9vbGVhbiB0cnVlIHwgZGViY29uZi1zZXQtc2VsZWN0aW9ucwogICAgZWNobyB1bmF0
dGVuZGVkLXVwZ3JhZGVzIHVuYXR0ZW5kZWQtdXBncmFkZXMvb3JpZ2luc19wYXR0ZXJuIHN0cmlu
ZyAib3JpZ2luPURlYmlhbixjb2RlbmFtZT1cJHtkaXN0cm9fY29kZW5hbWV9LGxhYmVsPURlYmlh
bi1TZWN1cml0eSIgfCBkZWJjb25mLXNldC1zZWxlY3Rpb25zCgogICAgbG9nICdVcGRhdGluZyBw
YWNrYWdlIGRhdGFiYXNlJwoKICAgIGFwdF9nZXQgdXBkYXRlCgogICAgaWYgW1sgJD8gLW5lIDAg
XV07IHRoZW4KICAgICAgICB3YXJuaW5nICdGYWlsZWQgdG8gdXBkYXRlIHBhY2thZ2UgZGF0YWJh
c2UnCiAgICBmaQoKICAgIGxvZyAnVXBncmFkaW5nIGV4aXN0aW5nIHBhY2thZ2VzJwoKICAgIGFw
dF9nZXQgdXBncmFkZQoKICAgIGlmIFtbICQ/IC1uZSAwIF1dOyB0aGVuCiAgICAgICAgd2Fybmlu
ZyAnRmFpbGVkIHRvIHVwZ3JhZGUgZXhpc3RpbmcgcGFja2FnZXMnCiAgICBmaQp9CgpmdW5jdGlv
biBwb3N0X2NvbmZpZ3VyZV9zeXN0ZW0KewogICAgbG9nICdQb3N0LWNvbmZpZ3VyaW5nIHN5c3Rl
bScKCiAgICBhcHRfZ2V0IGF1dG9yZW1vdmUKfQoKZnVuY3Rpb24gYWRkX3dpcmVndWFyZF9yZXBv
CnsKICAgIGxvZyAnQWRkaW5nIFdpcmVHdWFyZCByZXBvc2l0b3J5JwoKICAgIGFwdF9hZGRfcmVw
b3NpdG9yeSBwcGE6d2lyZWd1YXJkL3dpcmVndWFyZAp9CgpmdW5jdGlvbiBhZGRfdmVlYW1wbl9y
ZXBvCnsKICAgIGxvZyAnQWRkaW5nIFZlZWFtIFBOIHJlcG9zaXRvcnkga2V5JwoKICAgIGN1cmwg
LWsgLS1zaWxlbnQgImh0dHA6Ly9yZXBvc2l0b3J5LnZlZWFtLmNvbS9rZXlzL3ZlZWFtLmdwZyIg
fCBhcHQta2V5IGFkZCAtCgogICAgbG9nICdBZGRpbmcgVmVlYW0gUE4gcmVwb3NpdG9yeScKCiAg
ICBpbnN0YWxsIC1tIDA2NDQgLW8gcm9vdCAtZyByb290IC9kZXYvbnVsbCAvZXRjL2FwdC9zb3Vy
Y2VzLmxpc3QuZC92ZWVhbXBuLmxpc3QKCiAgICBlY2hvICJkZWIgW2FyY2g9YW1kNjRdIGh0dHA6
Ly9yZXBvc2l0b3J5LnZlZWFtLmNvbS9wbi9wdWJsaWMgcG4gc3RhYmxlIiA+IC9ldGMvYXB0L3Nv
dXJjZXMubGlzdC5kL3ZlZWFtcG4ubGlzdAp9CgpmdW5jdGlvbiBhZGRfcmVwb3NpdG9yaWVzCnsK
ICAgIGFkZF93aXJlZ3VhcmRfcmVwbwogICAgYWRkX3ZlZWFtcG5fcmVwbwoKICAgIGFwdF9nZXQg
dXBkYXRlCgogICAgaWYgW1sgJD8gLW5lIDAgXV07IHRoZW4KICAgICAgICBmYWlsICdGYWlsZWQg
dG8gYWRkIHJlcXVpcmVkIHJlcG9zaXRvcmllcycKICAgIGZpCn0KCmZ1bmN0aW9uIGluc3RhbGxf
cGFja2FnZXMKewogICAgbG9nICJJbnN0YWxsaW5nIFZlZWFtIFBOIgoKICAgICMgcHJldmVudCBB
cGFjaGUgYW5kIHVwZGF0ZXIgZnJvbSBzdGFydGluZyBhZnRlciBpbnN0YWxsYXRpb24KICAgIGNh
dCA+IC91c3Ivc2Jpbi9wb2xpY3ktcmMuZCA8PEVPRgojIS9iaW4vc2gKCmlmIFsgIlwkMSIgPSAi
YXBhY2hlMiIgXSB8fCBbICJcJDEiID0gInZlZWFtLXVwZGF0ZXIiIF07IHRoZW4KICAgIGV4aXQg
MTAxCmZpCgpleGl0IDAKRU9GCgogICAgY2htb2QgYSt4IC91c3Ivc2Jpbi9wb2xpY3ktcmMuZAoK
ICAgIGFwdF9nZXQgaW5zdGFsbCB2ZWVhbS12cG4tdWkgdmVlYW0tdnBuLXN2YwoKICAgIGlmIFtb
ICQ/IC1uZSAwIF1dOyB0aGVuCiAgICAgICAgZmFpbCAnRmFpbGVkIHRvIGluc3RhbGwgVmVlYW0g
UE4gcGFja2FnZXMnCiAgICBmaQp9CgpmdW5jdGlvbiBnZW5lcmF0ZV9zc2xfY2VydAp7CiAgICBs
b2cgJ1JlZ2VuZXJhdGluZyBTU0wgY2VydGlmaWNhdGUnCgogICAgdG91Y2ggL3Vzci9zaGFyZS92
ZWVhbXBuL2luaXRfc3NsCiAgICBzeXN0ZW1jdGwgc3RhcnQgdmVlYW1faW5pdF9TU0wKfQoKaWYg
W1sgJGFza19jb25maXJtYXRpb24gPT0gdHJ1ZSBdXTsgdGhlbgogICAgYXNrX2NvbmZpcm1hdGlv
bgpmaQoKd2ViX2xvZ19pbml0aWFsaXplCnBlcmZvcm1fcHJlX2NoZWNrcwpzdGFydF9pbml0X3dl
Yl9zaXRlCgpsb2cgIkluc3RhbGxpbmcgJHtwcm9kdWN0X25hbWV9IgoKaW5zdGFsbF9wcmVyZXF1
aXNpdGVzCgppZiBbWyAkY29uZmlndXJlX3N5c3RlbSA9PSB0cnVlIF1dOyB0aGVuCiAgICBwcmVf
Y29uZmlndXJlX3N5c3RlbQpmaQoKYWRkX3JlcG9zaXRvcmllcwppbnN0YWxsX3BhY2thZ2VzCmdl
bmVyYXRlX3NzbF9jZXJ0CgppZiBbWyAkY29uZmlndXJlX3N5c3RlbSA9PSB0cnVlIF1dOyB0aGVu
CiAgICBwb3N0X2NvbmZpZ3VyZV9zeXN0ZW0KZmkKCmxvZyAnSW5zdGFsbGF0aW9uIGNvbXBsZXRl
ZCcKCnN0YXJ0X21haW5fd2ViX3NpdGUAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAALi93ZWJfdXRpbHMuc2gAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAADAwMDA3NTUAMDAw
MDAwMAAwMDAwMDAwADAwMDAwMDAxNDU1ADEzNTY1NzQzNjAxADAxMjI2NAAgMAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAB1c3RhciAgAHJvb3QAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAcm9vdAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAjIS91c3IvYmluL2VudiBiYXNoCgojc2hlbGxjaGVjayBkaXNhYmxlPVNDMjAxNSxTQzIxNTUK
CltbIC1uICRIQVZFX1dFQl9VVElMUyBdXSAmJiByZXR1cm4gfHwgcmVhZG9ubHkgSEFWRV9XRUJf
VVRJTFM9MQoKcmVhZG9ubHkgVkVFQU1QTl9XRUJfU0VSVkVSX1BJRD0vdmFyL3J1bi92ZWVhbXBu
L2h0dHBkLnBpZAoKZnVuY3Rpb24gd2ViX3NlcnZlcl9zdGFydAp7CiAgICBsb2NhbCBTSVRFX1JP
T1Q9JDEKCiAgICBpZiBbWyAtZCAkU0lURV9ST09UIF1dOyB0aGVuCiAgICAgICAgcHVzaGQgIiRT
SVRFX1JPT1QiID4gL2Rldi9udWxsCgogICAgICAgIGlmIFtbICEgLWYgJFZFRUFNUE5fV0VCX1NF
UlZFUl9QSUQgXV07IHRoZW4KICAgICAgICAgICAgbWtkaXIgLXAgIiQoZGlybmFtZSAiJFZFRUFN
UE5fV0VCX1NFUlZFUl9QSUQiKSIKICAgICAgICAgICAgYnVzeWJveCBodHRwZCAtZiAtcCA4MCAt
dSBub2JvZHk6bm9ncm91cCAmCiAgICAgICAgICAgIGVjaG8gIiQhIiA+ICIkVkVFQU1QTl9XRUJf
U0VSVkVSX1BJRCIKICAgICAgICBmaQoKICAgICAgICBwb3BkID4gL2Rldi9udWxsCiAgICBmaQp9
CgpmdW5jdGlvbiB3ZWJfc2VydmVyX3N0b3AKewogICAgaWYgW1sgLWYgJFZFRUFNUE5fV0VCX1NF
UlZFUl9QSUQgXV07IHRoZW4KICAgICAgICBsb2NhbCBQSUQ9JChjYXQgIiRWRUVBTVBOX1dFQl9T
RVJWRVJfUElEIikKICAgICAgICBybSAtZiAiJFZFRUFNUE5fV0VCX1NFUlZFUl9QSUQiCiAgICAg
ICAga2lsbCAtVEVSTSAiJFBJRCIgMj4vZGV2L251bGwKICAgICAgICB3YWl0ICIkUElEIiAyPi9k
ZXYvbnVsbAogICAgZmkKfQoAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
Li9ydW5uaW5nLmdpZgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAADAwMDA2NDQAMDAwMDAw
MAAwMDAwMDAwADAwMDAwMDAzNDcxADEzNTY1NzQzNjAxADAxMjA3NwAgMAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAB1c3RhciAgAHJvb3QAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAcm9vdAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABH
SUY4OWEQABAA9AAA5eXlMzMz2trak5OTz8/PY2Njh4eHMzMzcHBwS0tLq6urt7e3QEBAn5+fNTU1
WFhYe3t7AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAIf8LTkVU
U0NBUEUyLjADAQAAACH+GkNyZWF0ZWQgd2l0aCBhamF4bG9hZC5pbmZvACH5BAkKAAAALAAAAAAQ
ABAAAAV3ICACAgkh5agCREIIx0GQq0gcieDIwqwSs8FhsAOmRILCEOZAIF62FEHpWPiQUKRA8bgi
VYOCs1EjCc7hQoFcO5/NC8MxqhIMGAVvzzoyJBp2A0AECiR8LDMEDwsKDQSCXyMCCAYADWQEgDUE
BjOXIgtzNQsLZSEAIfkECQoAAAAsAAAAABAAEAAABXYgIAICaUBlOY5EQQjJQRCMsorE8cAOES+r
GWBwaCQYjUNhJUAgBYzH6XBIqQgInRCweL1GAoHCdVOJFBCDAXgjhQUNBATCLr/dhIZZFGaOEQRM
Al8iAwUKcAozQoNXgQQQBAuIXXxMBg1cNpJ7fHqaOJ03WzchACH5BAkKAAAALAAAAAAQABAAAAV4
ICACAtkMZTmOBEQIRSIIj7KKhIPAMnIsqxegcVDEFIcCgAAUGGq0wmDwOBAGB+sSwkAQCDhiFkgS
LCAq0eIwGC2mDfBtRlcY7uQ5vUxQpEUzKwIKECiCAkIiDQYLZo17iCpCV18viX8CDUBfJImCfkty
gTdBcjchACH5BAkKAAAALAAAAAAQABAAAAV2ICACAtk0ZTmOhCEISPEiyyoSjxsL0FGPL0CDsYgt
DhASgTSAEAQFw6lwEDQOjFIr97wtEgef6LVwrQjIp2DRUCiWNgiD4aCdGvBVL/wgCQgLKiwEhCkE
AygrL10ibE+FL4YqQWuRf5OKC0uUjIpdQX42NqA2IQAh+QQJCgAAACwAAAAAEAAQAAAFbCAgAgJp
GGU5jgQiCMfxHssqEocbCwy9vgADLdY4MEgEEoKxgD1OO6EM0MoRVIRF7NAQvRauFe5xFRAa6KTt
sWWeDOpVb0syX1da8t2MVom0MWpnVwRlQHR1SC8vd2A/hYlmfiOSiZY2lJMjIQAh+QQJCgAAACwA
AAAAEAAQAAAFfiAgAgK5LGU5joIiCIPxGsQqCoY7DHBSsyUChLAjMAakmmChIMAaJ8NjmSikFgOU
SkBAHB6/F8G1EhQaLzHBaQM0EIjCYL1WrRqJBANC4r5WBA8NdX2EImMHBwVhTmkvDAddBwwpKY0t
BwgATGVsfwkHC21pfQAEBw1tZVsrIQAh+QQJCgAAACwAAAAAEAAQAAAFeSAgAgJJEGU5jsIiCE3z
NsQqwqgiN0XNloSBQEcoKEwm1HBxakBahaeJ+boRIAyE7yVArQQQBXesWikgEIRwbAMoCo+HgdSt
jggIcfV1KhMWBwdSJmMGEAkHYAcPKSkoDAcDB3N/X0qQBIg+X1wOBycOR203CIwkKyEAIfkECQoA
AAAsAAAAABAAEAAABXcgIAICSRBlOY4CKiyLICjEKr4o/EI1WxIxGAGyIKVastNJYWghBkeXqmV4
GHqy7EowiGW/NsDCQFZ8VasFBPE0urZEbbKFJSQOzZvrcBgMCglcBwVHAAN4CQkNfQBAKwR8BAwM
Ag8HPSsKBzGTLQlFYTUCbCkrIQAh+QQJCgAAACwAAAAAEAAQAAAFeSAgAgIpnOSonidBnEupkq/g
2o0sorb9DgSTiUVcKGyDnKkmOzUgjeAQNRJEicQZgDDoHrMzgmGsMC0SixWQtS0cDtEdocAYlASO
w4NwSDQUEAVWCRAlDWkDBwMPDwpwW1IifAkCjAIIDpEjCpkCCQ82BWlad2N3KiEAIfkECQoAAAAs
AAAAABAAEAAABXkgIAICKZzkqJ4sS5Qq+bbEAouoMJ8LYZo714vQsAFZOIFiYDTpbqQeC8KoQmIA
gmK7eBy+BSyxwdQRGb6RoLHLFr7NE+TRKAkSB8i9sB0gBAsIBiUKPgMHDQgFCwwKMioEBw8CigIG
D2kqCwkvBQU6EJkrJAMDdiohACH5BAkKAAAALAAAAAAQABAAAAV1ICACAimc5KiebFuqJOEKskoQ
cVujwnEMM5lgUVv4DjUTgfg6GQ6KlfAESVghMMByQUQ4GAxDdquQyRQF3EhQZmkRvuRpAFGUBI+D
QVBAcBsQQwYNJUQADVAGEAQPUTQ2DAUCigINaWNpAhAITmowdw2EKSMhACH5BAkKAAAALAAAAAAQ
ABAAAAV5ICACAimc5KguDHG+qAosRwILrkoQAHIorxxKkDgMCIdD8OQqEYqHhkK0LJkajoXqJhg8
CoWBjLTbGRJosQxXfi0QvJFg0SwJDIeW9aQYLOw+AwIQBjsKDThSTjwsCwOFEDwxVA8QXQYCCphj
BJGXJw1xayQKQCkjIQA7AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA==
